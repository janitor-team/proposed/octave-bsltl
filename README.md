# BioSpeckle Laser Tool Library (BSLTL)

To informations about the package, please visit the [doc/overview.html](./doc/overview.html) file.


To install from source code, execute in the command shell of linux:

	make install
	
from the current directory.