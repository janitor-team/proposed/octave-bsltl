%  Copyright (C) 2017, 2018   Fernando Pujaico Rivera
%
%  This file is a part of the Bio Speckle Laser Tool Library (BSLTL) package.
%
%  This BSLTL computer package is free software; you can redistribute it
%  and/or modify it under the terms of the GNU General Public License as
%  published by the Free Software Foundation; either version 3 of the
%  License, or (at your option) any later version.
%
%  This BSLTL computer package is distributed hoping that it could be
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, please download it from
%  <http://www.gnu.org/licenses>.

function MATW = mwnumbering(MAT,WLines,WColumns,varargin)
%
%  This function divides the MAT matrix in windows of WLines lines and WColumns 
%  columns, then in each one of these windows it is calculated the mean value of all 
%  elements. 
%
%  With theirs information is created a new matrix MATW with the same 
%  size and windows of MAT, and the mean values in the MAT matrix are replaced 
%  in all elements, for each window, in the MATW matrix.
%  
%  In the middle of each window is write the representative value of a window,
%  following the matrix MATW. 
%  
%  After starting the main routine just type the following command at the
%  prompt:
%  MATW = mwnumbering(MAT,WLines,WColumns);
%  MATW = mwnumbering(MAT,WLines,WColumns,FontSize);
%
%  Mean values, of the elements in the window of 8x10 pixels, in the MAT matrix.
%  In each windows is write the value of MATW in the window. 
%  The used font size is 10. 
%  MATW = mwnumbering(MAT,8,10);
%
%  Mean values, of the elements in the window of 8x10 pixels, in the MAT matrix.
%  In each windows is write the value of MATW in the window.
%  The used font size is 16. 
%  MATW = mwnumbering(MAT,8,10,16);
%  
%  Input:
%  MAT      is a matrix with NLIN lines and NCOL columns.
%  WLines   is the number of lines in each analysis window.
%  WColumns is the number of columns in each analysis window.
%  FontSize [Optional] The font size of text.
%
%  Output:
%  MATW     is a matrix with the mean values, of the elements in 
%           the window of WLinesxWColumns pixels, in the MAT matrix.
%
%
%  For help, bug reports and feature suggestions, please visit:
%  http://nongnu.org/bsltl/
%

%  Code developed by:  Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>
%  Code documented by: Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>
%
%  Date: 09 of July of 2018.
%
    NSIZE = size(MAT);
	NLIN  = NSIZE(1,1);
	NCOL  = NSIZE(1,2);

    if(nargin>3)
        if(isnumeric(varargin{1}))
            FONTSIZE=varargin{1};
        else
            error('The fourth parameter should be a number.')
        end
    else
        FONTSIZE=10;
    end

    MATW = mwindowing(MAT,WLines,WColumns);
    %figure
    imagesc(MATW)


    C=[1:WColumns:NCOL];
    L=[1:WLines:NLIN];
    [LL, CC] = meshgrid (L, C);

    for II=1:(size(LL,1)*size(LL,2))
        text(   CC(II)+WColumns/2, ...
                LL(II)+WLines/2, ...
                num2str(MATW(LL(II),CC(II)),"%5.1f"), ...
                'HorizontalAlignment','center','fontweight','bold','fontsize',FONTSIZE);
    end


end 
